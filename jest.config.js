
module.exports = {
    // Load setup-tests.js before test execution
    preset: "react-native",
    setupFilesAfterEnv: ['<rootDir>setup-tests.js'],
    transformIgnorePatterns: [
      'node_modules/(?!(jest-)?@?react-native|@react-native-community|@react-navigation)',
    ],
  };