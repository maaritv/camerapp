import React from 'react';
import { ImageCardComponent } from '../components/ImageCardComponent';

export class ImageScreen extends React.Component {


    constructor(props) {
        super(props);
    }


    render() {
        //console.log("props is "+JSON.stringify(this.props));
        const props = this.props.route.params;
        return (<ImageCardComponent {...props} />);
    }
}

export default ImageScreen;