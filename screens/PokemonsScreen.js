import React from 'react';
import { PokemonsGrid } from '../components/pokemons/PokemonsGrid';

export class PokemonsScreen extends React.Component {


    constructor(props) {
        super(props);
    }


    render() {
        //console.log("props is "+JSON.stringify(this.props));

        return ( <PokemonsGrid  {...this.props} />);
    }
}

