/**
 * @jest-environment jsdom
 */
import React from 'react';
import {
    ActivityIndicator,
    TouchableOpacity,
    Text,
    View,
    ScrollView,
    Image,
    SafeAreaView,
} from 'react-native';
import { mount, shallow, render } from 'enzyme';
import { PokemonComponent } from '../components/pokemons/PokemonComponent';
import { InfoTable, PokemonView } from '../components/pokemons/pokemoncomponents';
import { getPokemon } from '../services/rest/pokemonservice';

const pokemon = { id: 1, name: 'bulsaur', base_experience: 64, weight: 69, imageUrl: '', blobUrl: '' };
const imageUrl = 'https://pokeres.bastionbot.org/images/pokemon/1.png';

describe('Test PokemonComponent', () => {

    beforeEach(() => {
        jest.clearAllMocks();
        fetch.doMock();
    }
    )


    /**
     * You can mount class components and cause their lifecycle methods to be called.
     * You can also check their state.
     * 
     * If you do not know, what props are delivered e.g. by routing props, you can use 
     * console.log for logging them, where they are used in the app, and then use the 
     * same value as test props.
     */

    test('When PokemonComponent is mounted its loading state should be true and ActivityIndicator is present', () => {
        const wrapper = shallow(<PokemonComponent props={{ navigation: {}, route: { key: 'Pokemon-Hz-FG97PTCA_jO5vhc0cq', name: 'Pokemon', params: { url: 'https://pokeapi.co/api/v2/pokemon/1/', imageUrl: 'https://pokeres.bastionbot.org/images/pokemon/1.png' } } }} />);
        expect(wrapper.state("loading")).toEqual(true);
        expect(wrapper.find('ActivityIndicator').exists()).toBe(true);
    });

    test('When PokemonComponent pokemon state has pokemon set, error is false and loading is false PokemonView is present', () => {
        let wrapper = shallow(<PokemonComponent props={{ navigation: {}, route: { key: 'Pokemon-Hz-FG97PTCA_jO5vhc0cq', name: 'Pokemon', params: { url: 'https://pokeapi.co/api/v2/pokemon/1/', imageUrl: 'https://pokeres.bastionbot.org/images/pokemon/1.png' } } }} />);
        wrapper.setState({ loading: false, pokemon: pokemon, error: false });
        expect(wrapper.find('PokemonView').exists()).toBe(true);
    });

    test('When PokemonComponent has an error error text is present', () => {
        let wrapper = shallow(<PokemonComponent props={{ navigation: {}, route: { key: 'Pokemon-Hz-FG97PTCA_jO5vhc0cq', name: 'Pokemon', params: { url: 'https://pokeapi.co/api/v2/pokemon/1/', imageUrl: 'https://pokeres.bastionbot.org/images/pokemon/1.png' } } }} />);
        wrapper.setState({ loading: false, pokemon: pokemon, error: true });
        expect(wrapper.find('PokemonView').exists()).toBe(false);
        expect(wrapper.find('Text').exists()).toBe(true);
    });


    it("When PokemonComponent is mounted its componentDidMount should have been called once", () => {
        const spy = jest.spyOn(PokemonComponent.prototype, 'componentDidMount');  // spy on customFunc using the prototype
        const wrapper = mount(<PokemonComponent />);
        expect(spy).toHaveBeenCalledTimes(1);
    });

    /**
     * Mount a function component class and give properties to it.
     * Chech that props are as set.
     * Also check that component contains InfoTable -component.
     */

    it("When PokemonView is mounted its props should be correctly set", () => {
        const wrapper = mount(<PokemonView pokemon={pokemon}
            imageUrl={imageUrl} />);
        expect(wrapper.props().pokemon).toEqual(pokemon);
        expect(wrapper.props().imageUrl).toEqual(imageUrl);
    });

    it("When PokemonView is mounted it should contain an InfoTable and Image", () => {
        const wrapper = shallow(<PokemonView pokemon={pokemon} imageUrl={imageUrl} />);
        const infoTable = <InfoTable pokemon={pokemon} />;
        expect(wrapper.contains(infoTable)).toEqual(true);
        expect(wrapper.find('Image').exists()).toBe(true);
    });

    it("Info table must contain all fields correctly set", () => {
        const wrapper = shallow(<InfoTable pokemon={pokemon} />);
        expect(wrapper.find('Table').exists()).toBe(true);
        const table = wrapper.find('Table');
        expect(table.children.length).toEqual(1);
    });

})