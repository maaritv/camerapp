/**
 * @jest-environment jsdom
 */

import React from 'react';
import { mount, shallow, render } from 'enzyme';
import { PokemonsGrid } from '../components/pokemons/PokemonsGrid';
import { PokemonsView, IconTable } from '../components/pokemons/pokemongridcomponents';
// Note: test renderer must be required after react-native.
import renderer from 'react-test-renderer';

const pokemons = [{ id: 1, name: 'bulsaur', url: 'https://pokeapi.co/api/v2/pokemon/1' },
{ id: 2, name: 'ivysaur', url: 'https://pokeapi.co/api/v2/pokemon/2' }];




describe('Test PokemonGrid component', () => {

    beforeEach(() => {
        jest.clearAllMocks();
        fetch.doMock();
    }
    )

    it('PokemonGrid renders as expected', () => {
        const pokemonGrid = renderer.create(<PokemonsGrid />);
        expect(pokemonGrid).toMatchSnapshot();
    });


    test('When PokemonsGrid pokemons state has pokemons, component should contain one PokemonView', () => {
        let wrapper = shallow(<PokemonsGrid/>);
        wrapper.setState({ pokemons: pokemons, loading: false });
        expect(wrapper.find('PokemonsView').length).toBe(1)
    })

    /**
     * Mount PokemonsView and check that its props are set corrctly. 
     * 
     */

    it("When PokemonsView is mounted, its pokemons props should equal to given pokemon", () => {
        const showPokemon = jest.fn();

        const wrapper = mount(<PokemonsView pokemons={pokemons}
            showPokemon={showPokemon} />);

        expect(wrapper.props().pokemons).toEqual(pokemons);
    });

    /** With spy you can spy if class component member functions are called. 
     * You can not spy array functions or functions defined outside the class.
     */


    it("When PokemonsGrid is mounted its lifecylce methods should have been called", () => {
        const mountspy = jest.spyOn(PokemonsGrid.prototype, 'componentDidMount');  // spy on customFunc using the prototype
        const getdataspy = jest.spyOn(PokemonsGrid.prototype, 'getData');
        const renderspy = jest.spyOn(PokemonsGrid.prototype, 'render');  // spy on customFunc using the prototype
        const wrapper = mount(<PokemonsGrid />);

        expect(mountspy).toHaveBeenCalledTimes(1);
        expect(getdataspy).toHaveBeenCalledTimes(1);
        expect(renderspy).toHaveBeenCalledTimes(1);
    });

    /**
     * When class component is mounted, it should cause pokemons to be fetched. 
     * This uses npm jest-fetch-mock for mocking fetch i.e. actual REST call is not 
     * done.
     * 
     * We verify that:
     * - Only one call has been done
     * - Right url was called.
     */

    it('When PokemonsGrid is mounted fetch call to pokeapi is done once', () => {
        require('jest-fetch-mock').enableMocks()

        const mockResponse = pokemons;
        const setData = jest.fn();
        fetch.mockResponseOnce(JSON.stringify(mockResponse));

        const wrapper = mount(<PokemonsGrid />);

        expect(fetch.mock.calls.length).toEqual(1)
        expect(fetch.mock.calls[0][0]).toEqual('https://pokeapi.co/api/v2/pokemon?limit=50&offset=0')
    });

})