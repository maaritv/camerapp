/**
 * @format
 */

import { AppRegistry } from 'react-native';
import App from './App';
import { name as appName } from './app.json';
import { getRealm, getAllPokemonsWithData, getAllPokemons } from './services/local/pokemonstorageservice';
import { getFilePathForExtFile, writeImageToExtFile } from './services/local/fileserviceexternaldata';
import RNFetchBlob from 'react-native-fetch-blob';


let ImageFetch = async (event) => {
    console.log("!!!!!!!!!!!!!!!!!!!!!!!!!!FETCHING IMAGES!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
    const pokemons = getAllPokemons();
    let pokemonsWithData = getAllPokemonsWithData();
    pokemonsWithData.forEach((pokemon) => {
        if (pokemon.imageFile === '') {
            console.log("update " + pokemon.name);
            convertBlobToBase64(pokemon);
        }
    });
}

function updatePokemonImageFile(pokemon, imageFile) {
    console.log("=================UPDATING POKEMON ON BACKGROUND==================");
    console.log("======================" + imageFile + "==============================");
    let rlm = getRealm();
    rlm.write(() => {
        pokemon.imageFile = imageFile;
        pokemon.updated = new Date();
    })
}



function convertBlobToBase64(pokemon) {
    RNFetchBlob.fs.readFile(pokemon.imageFile, 'base64')
        .then((data) => {
            writeImageToExtFile(data, pokemon.id, 'png');
            updatePokemonImageFile(pokemon, getFilePathForExtFile(pokemon.id));
        })
}


//  AppRegistry.registerHeadlessTask('ImageFetchTask', () =>  require('ImageFetchTask'));

AppRegistry.registerHeadlessTask('ImageFetchTask', () =>
    ImageFetch
);

AppRegistry.registerComponent(appName, () => App);

