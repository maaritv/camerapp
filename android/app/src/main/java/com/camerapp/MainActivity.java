package com.camerapp;

import com.facebook.react.ReactActivity;
import android.util.Log;

public class MainActivity extends ReactActivity {

  /**
   * Returns the name of the main component registered from JavaScript. This is used to schedule
   * rendering of the component.
   */
  @Override
  protected String getMainComponentName() {
    return "camerapp";
  }

  @Override
  public void onStart() {
    super.onStart();
    Log.w("camerapp","^^^^^^^^^^^^^^^^^^^^^^^^^MAIN ACTIVITY onStart^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^");
    Log.w("camerapp","^^^^^^^^^^^^^^^^^^^^^^^^^MAIN ACTIVITY onStart^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^");
    Log.w("camerapp","^^^^^^^^^^^^^^^^^^^^^^^^^MAIN ACTIVITY onStart^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^");
    Log.w("camerapp","^^^^^^^^^^^^^^^^^^^^^^^^^MAIN ACTIVITY onStart^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^");
  }

  @Override
  public void onResume(){
    super.onResume();
    Log.w("camerapp","^^^^^^^^^^^^^^^^^^^^^^^^^MAIN ACTIVITY onResume^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^");
    Log.w("camerapp","^^^^^^^^^^^^^^^^^^^^^^^^^MAIN ACTIVITY onResume^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^");
    Log.w("camerapp","^^^^^^^^^^^^^^^^^^^^^^^^^MAIN ACTIVITY onResume^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^");
  }


  @Override
  public void onPause(){
    super.onPause();
    Log.w("camerapp","^^^^^^^^^^^^^^^^^^^^^^^^^MAIN ACTIVITY onPause^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^");
    Log.w("camerapp","^^^^^^^^^^^^^^^^^^^^^^^^^MAIN ACTIVITY onPause^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^");
    Log.w("camerapp","^^^^^^^^^^^^^^^^^^^^^^^^^MAIN ACTIVITY onPause^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^");
  }


  @Override
  public void onStop(){
    super.onStop();
    Log.w("camerapp","^^^^^^^^^^^^^^^^^^^^^^^^^MAIN ACTIVITYonStop^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^");
    Log.w("camerapp","^^^^^^^^^^^^^^^^^^^^^^^^^MAIN ACTIVITY onStop^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^");
    Log.w("camerapp","^^^^^^^^^^^^^^^^^^^^^^^^^MAIN ACTIVITY onStop^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^");
  }

  @Override
  public void onDestroy(){
    super.onDestroy();
    Log.w("camerapp","^^^^^^^^^^^^^^^^^^^^^^^^^MAIN ACTIVITY onDestroy^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^");
    Log.w("camerapp","^^^^^^^^^^^^^^^^^^^^^^^^^MAIN ACTIVITY onDestroy^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^");
    Log.w("camerapp","^^^^^^^^^^^^^^^^^^^^^^^^^MAIN ACTIVITY onDestroy^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^");
  }


}
