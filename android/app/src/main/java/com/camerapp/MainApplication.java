package com.camerapp;

import android.app.Application;
import android.content.Context;
import com.facebook.react.PackageList;
import com.facebook.react.ReactApplication;
import org.reactnative.camera.RNCameraPackage;
import com.rnfs.RNFSPackage;
import com.facebook.react.ReactInstanceManager;
import com.facebook.react.ReactNativeHost;
import com.facebook.react.ReactPackage;
import com.facebook.soloader.SoLoader;
import java.lang.reflect.InvocationTargetException;
import java.util.List;
import android.os.Bundle;
import android.content.Intent;
import com.facebook.react.HeadlessJsTaskService;
import android.util.Log;


public class MainApplication extends Application implements ReactApplication {

  private final ReactNativeHost mReactNativeHost = new ReactNativeHost(this) {
    @Override
    public boolean getUseDeveloperSupport() {
      return BuildConfig.DEBUG;
    }

    @Override
    protected List<ReactPackage> getPackages() {
      @SuppressWarnings("UnnecessaryLocalVariable")
      List<ReactPackage> packages = new PackageList(this).getPackages();
      // Packages that cannot be autolinked yet can be added manually here, for
      // example:
      // packages.add(new MyReactNativePackage());
      return packages;
    }

    @Override
    protected String getJSMainModuleName() {
      return "index";
    }
  };

  @Override
  public ReactNativeHost getReactNativeHost() {
    return mReactNativeHost;
  }

 @Override
  public void onCreate() {
    super.onCreate();
    SoLoader.init(this,  false);
    initializeFlipper(this, getReactNativeHost().getReactInstanceManager());
    Log.w("camerapp","^^^^^^^^^^^^^^^^^^^^^^^^^CAMERA APP STARTED^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^");
    Log.w("camerapp","^^^^^^^^^^^^^^^^^^^^^^^^^CAMERA APP STARTED^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^");
    Log.w("camerapp","^^^^^^^^^^^^^^^^^^^^^^^^^CAMERA APP STARTED^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^");
    Log.w("camerapp","^^^^^^^^^^^^^^^^^^^^^^^^^CAMERA APP STARTED^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^");
  }

  @Override
  public void onLowMemory(){
    super.onLowMemory();
    Log.w("camerapp","^^^^^^^^^^^^^^^^^^^^^^^^^CAMERA APP LOW ON MEMORY^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^");
    Log.w("camerapp","^^^^^^^^^^^^^^^^^^^^^^^^^CAMERA APP LOW ON MEMORY^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^");
    Log.w("camerapp","^^^^^^^^^^^^^^^^^^^^^^^^^CAMERA APP LOW ON MEMORY^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^");
  }


  @Override
  public void onTerminate(){
    super.onTerminate();
    Log.w("camerapp","^^^^^^^^^^^^^^^^^^^^^^^^^CAMERA APP WAS TERMINATED^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^");
    Log.w("camerapp","^^^^^^^^^^^^^^^^^^^^^^^^^CAMERA APP WAS TERMINATED^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^");
    Log.w("camerapp","^^^^^^^^^^^^^^^^^^^^^^^^^CAMERA APP WAS TERMINATED^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^");
  }
  

  /**
   * Loads Flipper in React Native templates. Call this in the onCreate method
   * with something like initializeFlipper(this,
   * getReactNativeHost().getReactInstanceManager());
   *
   * @param context
   * @param reactInstanceManager
   */
  private static void initializeFlipper(Context context, ReactInstanceManager reactInstanceManager) {
    if (BuildConfig.DEBUG) {
      try {
        /*
         * We use reflection here to pick up the class that initializes Flipper, since
         * Flipper library is not available in release mode
         */
        Class<?> aClass = Class.forName("com.camerapp.ReactNativeFlipper");
        aClass.getMethod("initializeFlipper", Context.class, ReactInstanceManager.class).invoke(null, context,
            reactInstanceManager);
      } catch (ClassNotFoundException e) {
        e.printStackTrace();
      } catch (NoSuchMethodException e) {
        e.printStackTrace();
      } catch (IllegalAccessException e) {
        e.printStackTrace();
      } catch (InvocationTargetException e) {
        e.printStackTrace();
      }
    }
  }
}
