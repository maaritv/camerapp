package com.camerapp;
import android.content.Intent;
import android.os.Bundle;
import com.facebook.react.HeadlessJsTaskService;
import com.facebook.react.bridge.Arguments;
import com.facebook.react.jstasks.HeadlessJsTaskConfig;
import javax.annotation.Nullable;
import android.util.Log;
import com.facebook.react.HeadlessJsTaskService;

/**     <!--service android:name="com.camerapp.PokemonTaskService" /-->

      <!--receiver android:name="com.camerapp.NetworkChangeReceiver" -->
      <intent-filter>
      <action android:name="android.net.conn.CONNECTIVITY_CHANGE" />
      </intent-filter>
      </receiver>

       */

public class PokemonTaskService extends HeadlessJsTaskService {

  @Override
  protected @Nullable HeadlessJsTaskConfig getTaskConfig(Intent intent) {
    Log.w("pokemonservice","######################################starting task pokemon task service!!####################################");
    Bundle extras = intent.getExtras();
     if (extras != null) {
      Log.w("pokemonservice", "starting task pokemon task service!!????????????????????????????");
      return new HeadlessJsTaskConfig(
          "ImageFetchTask",
          Arguments.fromBundle(extras),
          5000, // timeout for the task
          false // optional: defines whether or not  the task is allowed in foreground. Default is false
        );
    }
    return null;
  }




}