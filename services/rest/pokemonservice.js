

export async function getPokemons(offset, pageNumber) {
    console.log("Get data from rest");
    try {
        const pokemonsJSON = await fetch(
            "https://pokeapi.co/api/v2/pokemon?limit=" + offset + "&offset=" + offset * pageNumber
        )
            .then((response) => response.json())
            .then((data) => {
                //console.log(JSON.stringify(data.results));
                return data.results;
            })
        return pokemonsJSON;
    }
    catch (exception) {
        console.log("error");
        return [];
    }
}

