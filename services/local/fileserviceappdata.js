var RNFS = require('react-native-fs');

// create a path you want to write to
// :warning: on iOS, you cannot write into `RNFS.MainBundlePath`,
// but `RNFS.DocumentDirectoryPath` exists on both platforms and is writable


/**
 * Writes base64 content to file without base64-prefix. It appears to app data folder 
 * like /data/user/0/com.camera_sample/files/
 * 
 * @param content 
 */


export async function writeTxtFile(content, filePath) {
    var path = RNFS.DocumentDirectoryPath + '/' + filePath;

    // write the file
    const status = await RNFS.writeFile(path, content)
        .then((success) => {
            console.log("data is stored to " + path);
            return 0;
        })
        .catch((err) => {
            console.log(err.message);
            return 1;
        });
        console.log("return status "+status);
        return status;
}



export async function readTxtFile(fileName) {
    var filePath = RNFS.DocumentDirectoryPath + '/' + fileName;
    let data = await RNFS.readFile(filePath);
    return data;
}
