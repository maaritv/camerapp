var RNFS = require('react-native-fs');



export function getPath() {
    //return RNFS.PicturesDirectoryPath +folder;
    return RNFS.PicturesDirectoryPath + "/";
}

export function getFilePath(fileName) {
    return getPath() + fileName + ".jpg";
}

/**
 * Writes base64 content to file with base64-prefix. It appears to internal storage 
 * to pokeappi-folder and you can browse it with file manager app. (/storage/emulated/0/Android/data/com.camera_sample/files/pokeappi/)
 * 
 * @param content 
 */

export async function writeImageToExtFile(content, id, format) {
    var path = getFilePath(id);
    try {
        RNFS.mkdir(getPath());
        console.log("path is created " + path);
    }
    catch (exception) {
        console.log("path was not created " + exception.message);
    }


    // write the file
    const status = await RNFS.writeFile(path, 'data:image/' + format + ';base64,' + content, 'base64')
        .then((success) => {
            //console.log("data " + content + " is stored to " + path);
            return 0;
        })
        .catch((err) => {
            console.log(err.message);
            return 1;
        });
    return status;
}



export async function readImageFromExtFile(imageFile) {
    let data = await RNFS.readFile(imageFile, 'base64');
    return data;
}


