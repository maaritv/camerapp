import 'react-native-gesture-handler';
import React from 'react';
import { Text, SafeAreaView, StyleSheet, StatusBar, FlatList, View } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import DropdownAlert from 'react-native-dropdownalert';
import { ImageScreen } from './screens/ImageScreen';
import { PokemonsScreen } from './screens/PokemonsScreen';
import { PokemonScreen } from './screens/PokemonScreen';
import { CameraScreen } from './screens/CameraScreen';

import { writeTxtFile, readTxtFile } from './services/local/fileserviceappdata';


class HomeScreen extends React.Component {

  constructor(props) {
    super(props);
  }

  async componentDidMount() {

    //Example how to write file to internal storage, which is not shared with other 
    //apps but is not also best option for higly critical data. Use safe storage for that.
    const license = "license2.txt";
    const writeStatus = await writeTxtFile("Pokemon app uses free PokeAPI. Sprites are available via pokemondb.net https://pokemondb.net/about#privacy", license)
    console.log("Writing license status returned " + writeStatus);
    
    if (writeStatus == 1) {
      this.dropDownAlertRef.alertWithType('error', 'Error writing license text');
    }
    try {
      const readPromise = readTxtFile(license);
      console.log("We are now waiting for function to complete");
      const readStatus = await readPromise;
      console.log("Reading license returned:" + readStatus);
    }
    catch (exception){
      this.dropDownAlertRef.alertWithType('error', 'Error reading license text', exception);
    }
    
  }

  // DropdownAlert must be last component in the document tree.
  // This ensures that it overlaps other UI components.
  //https://www.npmjs.com/package/react-native-dropdownalert

  render() {
    return (<View>
      <View style={styles.item}>

        <Text style={{ fontSize: 20 }}
          onPress={() =>
            this.props.navigation.push('Camera', {
              navigation: this.props.navigation,
            })
          }>
          Camera
  </Text>
        <Text style={{ fontSize: 20 }}
          onPress={() =>
            this.props.navigation.push('Pokemons', {
              navigation: this.props.navigation,
            })
          }>
          Pokemons
  </Text>
      </View>
      <View>
        <DropdownAlert ref={ref => this.dropDownAlertRef = ref} />
      </View>
    </View>);
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: StatusBar.currentHeight || 0,
  },
  item: {
    backgroundColor: '#f9c2ff',
    padding: 20,
    marginVertical: 8,
    marginHorizontal: 16,
  },
  title: {
    fontSize: 32,
  },
});


export const App = () => {

  const Stack = createStackNavigator();

  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen
          name="Home"
          component={HomeScreen}
          options={{ title: 'Home' }}
        />
        <Stack.Screen
          name="Image"
          component={ImageScreen}
          options={{ title: 'Image' }}
        />
        <Stack.Screen
          name="Camera"
          component={CameraScreen}
          options={{ title: 'Camera' }}
        />
        <Stack.Screen
          name="Pokemons"
          component={PokemonsScreen}
          options={{ title: 'Pokemons' }}
        />
        <Stack.Screen
          name="Pokemon"
          component={PokemonScreen}
          options={{ title: 'Pokemon' }}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default App
