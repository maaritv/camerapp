import React from 'react';
import { Text, SafeAreaView, StyleSheet, StatusBar, FlatList, View } from 'react-native';
import { ImageCard } from './ImageCard';


const renderItem = ({ item }) => (
    <Item name={item.name} />
);

const Item = ({ name }) => (
    <View style={styles.item}>
        <Text style={styles.title}>{name}</Text>
    </View>
);


export class ImageCardComponent extends React.Component {


    constructor(props) {
        super(props);
        this.state = {
            image: {},
            name: "",
            description: ""
        }
    }


    async componentDidMount() {
        const image = this.props.image;
        console.log(this.props.path);
        console.log("image is " + JSON.stringify(image));
        this.setState({ image: image });
        this.setState({ name: "fileName" })
        this.setState({ description: "Kuvaus " })
    }


    render() {
        return (
            <ImageCard name={this.state.name} navigation={this.props.navigation} description={this.state.description} image={this.state.image} />
        )
    }

}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginTop: StatusBar.currentHeight || 0,
    },
    item: {
        backgroundColor: '#f9c2ff',
        padding: 20,
        marginVertical: 8,
        marginHorizontal: 16,
    },
    title: {
        fontSize: 32,
    },
});