import React from 'react';
import {
    ActivityIndicator,
    TouchableOpacity,
    Text,
    View,
    ScrollView,
    Image,
    SafeAreaView,
} from 'react-native';
import DropdownAlert from 'react-native-dropdownalert';
import { getAllPokemons, writeAllPokemons, deleteAllPokemons } from '../../services/local/pokemonstorageservice';
import { getPokemons } from '../../services/rest/pokemonservice';
import { PokemonsView } from './pokemongridcomponents';
import { getAndSaveBinaryData, getFilePathForBlob } from '../../services/local/blobfileservice';


export class PokemonsGrid extends React.Component {


    constructor(props) {
        super(props);
        console.log(JSON.stringify(this.props));
        this.state = {
            pokemons: [],
            flagData: '',
            loading: true,
            error: false,
            plainmode: false,
            currentPage: 0
        };
    }


    showPokemon = (id, url, imageFile) => {
        this.props.navigation.push('Pokemon', {
            id: id,
            url: url,
            imageFile: imageFile,
            navigation: this.props.navigation.route,
        })
    }


    async getData(pageNumber) {
        const imageExtension = "png";
        const pokemons = await getPokemons(50, pageNumber).then(pokemons => {
            if (pokemons.length == 0) {
                this.dropDownAlertRef.alertWithType('error', 'Could not get pokemons from net');
            }
            if (pokemons !== undefined) {
                //This is error situation. You can also use try/catch and leave error unhandled.
                deleteAllPokemons();
                pokemons.map(pokemon => {
                    const blobFileName = getFilePathForBlob(pokemon.name + "." + imageExtension);
                    pokemon.imageFile = blobFileName;
                })
                writeAllPokemons(pokemons);
            }
            return pokemons;
        })
        await this.saveImagesOnBackground(pokemons);
        return pokemons;
    }

    async saveImagesOnBackground(pokemons) {
        pokemons.forEach(async (pokemon) => {
            const imageUrl = `https://img.pokemondb.net/sprites/x-y/normal/${pokemon.name}.png`;
            const status = await getAndSaveBinaryData(imageUrl, pokemon.imageFile);
            //console.log("status " + status + " for " + imageUrl);
        })
    }

    async componentDidMount() {
        let pokemons = getAllPokemons();

        //Show pokemons from storage, if they are there.
        if (pokemons.length === 0) {
            //load from REST because they are not yet stored to REALM DB.
            console.log("Getting pokemons from REST API");
            pokemons = await this.getData(0);
            this.setState({
                pokemons: pokemons,
                loading: false,
            });
        }
        else {
            //Found Pokemons in Realm DB
            //console.log("Realm pokemons are " + JSON.stringify(pokemons));
            this.setState({
                pokemons: pokemons,
                loading: false,
            });

        }

    }

    render() {
        if (this.state.loading) {
            return <View><ActivityIndicator size="large" color="#00ff00" />
                <DropdownAlert ref={ref => this.dropDownAlertRef = ref} />
            </View>;
        }
        else if (this.state.pokemons.length == 0) {
            return (
                <View>
                    <Text>No pokemons found from net or storage. Check that you are online!</Text>
                    <DropdownAlert ref={ref => this.dropDownAlertRef = ref} />
                </View>
            );
        } else {
            return (
                <PokemonsView pokemons={this.state.pokemons} showPokemon={this.showPokemon} />
            );
        }
    }
}
