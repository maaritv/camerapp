import React from 'react';
import {
  ActivityIndicator,
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
} from 'react-native';
import { addPokemon, getPokemonById } from '../../services/local/pokemonstorageservice';
import { PokemonView } from './pokemoncomponents';
import axios from 'axios';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 16,
    paddingTop: 30,
    backgroundColor: '#fff',
    margin: 'auto',
  },
  tinyLogo: {
    marginLeft: 'auto',
    marginRight: 'auto',
    width: 300,
    height: 180,
    marginBottom: 20,
  },
});



export class PokemonComponent extends React.Component {
  constructor(props) {
    super(props);
    console.log("-----------------CONSTRUCTOR-" + JSON.stringify(this.props) + "--------------------");
    this.state = {
      pokemon: {},
      flagData: '',
      loading: true,
      error: false,
      plainmode: false,
      errormessage: ''
    };
  }


  componentWillUnmount() {
    console.log("-------------component will unmount-------------------------------");
  }

  shouldComponentUpdate(nextProps, nextState) {
    console.log("------------- should component update -" + this.state.pokemon.name + "/" + JSON.stringify(nextState.pokemon.name) + "------------------------------");
    if (nextState.pokemon.name !== this.state.pokemon.name) {
      return true;
    }
    return false;
  }

  static getDerivedStateFromError(error) {
    // Update state so the next render will show the fallback UI.
    console.log("-------------error happended-" + this.state.pokemon.name + "------------------------------");
    return { errormessage: error };
  }

  addPokemonToDB = async (pokemon) => {
    let status = await addPokemon(pokemon);
    this.setState({ pokemon, imageFile: pokemon.imageFile, loading: false });
  }


  async componentDidMount() {
    console.log("-----------------component did mount-" + this.props.route.params.id + "--------------------");
    //Try get fetch Pokemon first from Realm DB. If it does not exist, fetch it from REST-service.
    let pokemon = getPokemonById(this.props.route.params.id);
    if (pokemon) {
      console.log("got pokemon from storage "+JSON.stringify(pokemon));
      this.setState({ pokemon: pokemon, imageFile: pokemon.imageFile, loading: false });
    }
    else {
      console.log("try fetch pokemon from net");
      axios.get(this.props.route.params.url)
        .then(res => {
            let pokemon = res.data;
            pokemon.imageFile = this.props.route.params.imageFile;
            this.addPokemonToDB(pokemon)
        })
  }
}



  render() {
    console.log("----------------rendering pokemon " + this.state.pokemon.name + "!!!----------------------");
    if (this.state.error === true) {
      return (
        <View>
          <Text>Error {this.state.errormessage} happened.</Text>
        </View>
      );
    }
    else if (this.state.loading === true) {
      return <ActivityIndicator size="large" color="#00ff00" />;
    } else {
      return (
        <PokemonView pokemon={this.state.pokemon} imageFile={this.state.imageFile}/>
      );
    }
  }
}
