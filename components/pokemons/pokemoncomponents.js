import React from 'react';
import {
  ActivityIndicator,
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
} from 'react-native';
import { Table, Rows } from 'react-native-table-component';



const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 16,
    paddingTop: 30,
    backgroundColor: '#fff',
    margin: 'auto',
  },
  tinyLogo: {
    marginLeft: 'auto',
    marginRight: 'auto',
    width: 300,
    height: 180,
    marginBottom: 20,
  },
});


export const InfoTable = ({ pokemon }) => {
  const tableData = [
    ['ID', pokemon.id],
    ['Name', pokemon.name],
    ['Base experience', pokemon.base_experience],
    ['Weight', pokemon.weight],
    ['Image file', pokemon.imageFile]
  ];

  return (
    <View style={styles.container}>
      <Table borderStyle={{ borderWidth: 1, borderColor: '#000000' }}>
        <Rows data={tableData} />
      </Table>
    </View>
  );
};

export const PokemonView = ({ pokemon, imageFile }) => {
return (
      <View style={styles.container}>
        <Image
          style={{ height: 120, width: 120, padding: 20, alignSelf: 'center' }}
          source={{
            uri: 'file://'+imageFile
          }}
        />
        <InfoTable pokemon={pokemon} />

      </View>
    );
};