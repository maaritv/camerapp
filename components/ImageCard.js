import * as React from 'react';
import { Text, View, Button, SafeAreaView, StyleSheet, Image, Dimensions } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';

export const ImageCardImage = ({ data }) => {
  const dimensions = Dimensions.get('window');
  const imageHeight = Math.round(dimensions.height / 3);
  //const imageWidth = dimensions.width;

  //uri: 'data:image/jpeg;base64,' + this.state.fileData
  //uri: imageUrl

  return (<View style={{ backgroundColor: '#CCCCCC' }}>
    <Image style={{ height: imageHeight }} source={{ uri: data.uri }}
    />
  </View>);
}

export const ImageCardText = ({ name, text }) => {
  return (<View>
    <Text style={styles.title}>
      {name}
    </Text>
    <Text style={styles.paragraph}>
      {text}
    </Text>
  </View>);
}

const ImageCardButton = ({ saveImage, image }) => {

  return (<View><TouchableOpacity style={styles.button} onPress={() => saveImage(image.uri)} >
     <Text style={{ fontSize: 14 }}> Save image to Gallery</Text>
    </TouchableOpacity></View>);
}


export const ImageCard = ({name, navigation, description, image }) => {
  //console.log("data "+JSON.stringify(image));
  return (
    <SafeAreaView style={styles.container}>
      <ImageCardImage data={image} />
      <ImageCardText name={name} text={description} />
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 0,
    backgroundColor: '#EEEEEE',
  },
  paragraph: {
    margin: 24,
    fontSize: 14,
    fontWeight: 'normal',
    textAlign: 'left',
  },
  title: {
    margin: 28,
    fontSize: 18,
    fontWeight: 'bold',
    textAlign: 'center',
  },
  button: {
    flex: 0,
    backgroundColor: '#fff',
    borderRadius: 5,
    padding: 15,
    paddingHorizontal: 20,
    alignSelf: 'center',
    margin: 20,
},
  image: {
    height: 100
  }
});

