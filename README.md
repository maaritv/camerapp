Camera app
=======================================================
   * Uses react-native-camera
   * Has REST client, which stores data to Realm -object database
   * Stores images to Pictures -folder (available in Android Gallery)
   * Uses Realm for storing and retrieving structured data 

